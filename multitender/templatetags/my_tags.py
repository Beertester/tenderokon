# -*- coding: utf-8 -*-
from django import template
from django.utils.datastructures import SortedDict
import datetime

register = template.Library()

@register.filter(name='sort')
def listsort(value, arg):
        if isinstance(value, dict):
            new_dict = SortedDict()
            key_list_l = value.keys()

            if arg == 2:
                key_list = sorted(key_list_l, reverse = True)
            else:
                key_list = sorted(key_list_l)

            for key in key_list:
                new_dict[key] = value[key]
            return new_dict
        elif isinstance(value, list):
            new_list_l = list(value)
            if arg == 2:
                new_list = sorted(new_list_l, reverse = True)
            else:
                new_list = sorted(new_list_l)
            return new_list
        else:
            return value
        listsort.is_safe = True

@register.filter
def typev(value):
    return type(value)

@register.filter
def verbose_name(value):
    return value._meta.verbose_name

@register.filter
def replace_comma(value):
    return str(value).replace(',', '.')

@register.filter
def finish_in_tender(value):
    """Отдает True если НЕ имеется дата окончания тендера"""
    for v in value:
        try:
            v.datetime_finish
        except:
            result = True
            break
        else:
            if v.datetime_finish == None:
                result = True
                break
            else:
                result = False
    return result


@register.filter
def finish_notin_tender(value):
    """Отдает True если имеется дата окончания тендера"""
    for v in value:
        try:
            v.datetime_finish
        except:
            result = False
        else:
            if v.datetime_finish == None:
                result = False
            else:
                result = True                
                break
    return result

@register.filter
def finish_comment_notin_tender(value):
    """Отдает True если НЕ имеется коментарий к тенедеру"""
    for v in value:
        try:
            v.final_comment
        except:
            result = True
            break
        else:
            try:
               v.final_comment.datetime_add
            except:
                result = True
                break
            else:
                result = False
        # print v.id, ' - ', result
    return result

@register.filter
def is_today(value):
    try:
        value_to_date = value.date()
    except:
        return False
    else:    
        if value_to_date == datetime.datetime.today().date():
            return True
        else:
            return False

@register.filter
def min_to_time(value):
    try:
        int_val = int(value)
    except:
        return value
    else:
        if int_val <= 60:
            result = u'%sм' % int_val
        elif int_val > 60 and int_val <= 1440:
            result = u'%sч %sм' % (int_val/60, int_val%60)
        else:
            result = u'%sд %sч' % ((int_val/60)/24, (int_val/60)%24)

        return result

@register.filter
def state_in(value, arg):
    list_of_state = arg.split(',')

    for v in value:
        for st in list_of_state:
            try: 
                v.state[0].state
            except:
                if 'new' in list_of_state:
                    # print "Есть!",list_of_state
                    result = True
                    break
                else:
                    # print "нету",list_of_state
                    result = False
            else:
                if st == v.state[0].state:
                    result = True
                    break
                else:
                    result = False
        if result == True:
            break

    return result

@register.filter
def in_the_party(value):
    for v in value:
        try:
            v.party_count
        except:
            result = False
        else:
            if v.party_count > 0:
                result = True
                break
            else:
                result = False

    return result

@register.filter
def tupo_zav(value):
    """Универсально не выходит - придется тупо!"""

    for v in value:
        try:
            v.state[0].state
        except:
            result = False
        else:
            # print v.id, "- ", v.state[0].state, " - ", v.party_count
            if ((v.state[0].state == 'tclose' or v.state[0].state == 'uclose') and
                v.party_count > 0):
                # print "Ура!", v.id
                result = True
                break
            else:
                result = False

    return result

# @register.filter
# def state_not_in(value, arg):
#     list_of_state = arg.split(',')

#     for state in list_of_state:
#         for v in value:
#             try: 
#                 value.state.0.state
#             except:
#                 if 'new' in list_of_state:
#                     result = True
#                 else:
#                     result = True
#                     break
#             else:
#                 if state == value.state.0.state:
#                     result = True
#                     break
#                 else:
#                     result = False

#     return result
