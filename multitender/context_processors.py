# -*- coding: utf-8 -*-
from django.template import Context, loader, RequestContext
from accounts.models import *

def banners(request):
    try:
        banner = CompanyAdversImage.objects.all().order_by('?')[0]
    except:
        banner = False
    return {'banner': banner}
