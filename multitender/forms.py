# -*- coding: utf-8 -*-
from django import forms
from multitender.models import *

class FeedbackForm(forms.ModelForm):
    from_adress = forms.CharField(label=u'Куда отвечать',
            widget=forms.TextInput(
                attrs={"placeholder": u'to_me@mail.ru', "class": u'full-width',}
            ),
            required=False
        )

    article = forms.CharField(label=u'Сообщение:',
            widget=forms.Textarea(
                attrs={"rows": 3, "class": u'full-width',}
            ),
            required=False
        )  

    class Meta:
        model = Feedback
        exclude = ['datetime_add', ]