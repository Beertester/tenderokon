# -*- coding: utf-8 -*-
from django.contrib import admin
from accounts.models import *
from tender.models import *

admin.site.register(Company)
admin.site.register(Tender)
