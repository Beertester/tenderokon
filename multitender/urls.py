# -*- coding: utf-8 -*-
import os
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.conf import settings 
from django.contrib import admin
from accounts.forms import *
from tender import urls as tender_urls
# from accounts.views import *
# import accounts

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'multitender.views.home', name='home'),
    url(r'^prices/$', 'multitender.views.price_page', name='price_page'),
    url(r'^get_citys/$','multitender.views.get_citys', name='get_citys'),
    url(r'^check_mail/$','multitender.views.check_mail', name='check_mail'),
    url(r'^ajax_login/$','multitender.views.ajax_login', name='ajax_login'),
    url(r'^feedback/$','multitender.views.feedback_page', name='feedback_page'),
    url(r'^howitwork/$','multitender.views.howitwork', name='howitwork'),

    # url(r'^multitender/', include('multitender.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^media/(.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^accounts/signup/$', 'accounts.views.my_signup', {
        'signup_form': MainUserSignupForm, 
        'template_name': 'signup.html',
        'success_url': '/tender/add/',
    }, name='userena_signup'),
    url(r'^accounts/(?P<username>[\.\w-]+)/edit/$', 'accounts.views.my_profile_edit', {
        'edit_profile_form': MyEditProfileForm,
    }, name='userena_profile_edit'),
    # url(r'^accounts/signup/$', 'userena.views.signup', {'signup_form': MainUserSignupForm}, name='userena_signup'),
    url(r'^company/', include('accounts.urls')),    
    url(r'^accounts/', include('userena.urls')),
    url(r'^tender/', include('tender.urls')),
    url(r'^geoip/', include('django_geoip.urls')),
)

# handler500 = 'renta.views.my_page_not_found'
# handler404 = 'renta.views.my_page_not_found'

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^404$', TemplateView.as_view(template_name='404.html')),
        url(r'^500$', TemplateView.as_view(template_name='500.html')),
    )