# -*- coding: utf-8 -*-
# Django settings for multitender project.

import os
import django
import os.path
import posixpath
# calculated paths for django and the site
# used as starting points for various other paths
# DJANGO_ROOT = os.path.dirname(os.path.realpath(django.__package__))
# SITE_ROOT = os.path.dirname(os.path.realpath(__package__))
SITE_ROOT = u'/home/y/yogina/tenderokon.ru/public_html/'
# PROJECT_DIR = os.path.dirname(__package__)

DEBUG = TEMPLATE_DEBUG = False
# DEBUG = True
# TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Aleksey', 'yoginalex@gmail.com'),
)

MANAGERS = ADMINS
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
#         'NAME': '/home/y/yogina/tenderokon.ru/public_html/db.sqlite',                      # Or path to database file if using sqlite3.
#         'USER': '',                      # Not used with sqlite3.
#         'PASSWORD': '',                  # Not used with sqlite3.
#         'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
#         'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'yogina_tenokon',                      # Or path to database file if using sqlite3.
        'USER': 'yogina_tenokon',                      # Not used with sqlite3.
        'PASSWORD': 'yieHfemhR8',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}



# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Moscow'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ru-RU'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

THOUSAND_SEPARATOR = ' '

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.path.join(SITE_ROOT, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
# STATIC_ROOT = '/home/y/yogina/multitender.ru/public_html/static/'
# STATIC_ROOT = ''
STATIC_ROOT = os.path.join(SITE_ROOT, 'static')

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # "/home/y/yogina/renta.alexportfolio.ru/public_html/renta/static"
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(SITE_ROOT, 'templates/static'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'deeb@8wz6yz0i3i%kqtbj5q5c6@u=i1cr($)!ud)s4z@blcm0d'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

AUTHENTICATION_BACKENDS = (
    'userena.backends.UserenaAuthenticationBackend',
    'guardian.backends.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
)
ANONYMOUS_USER_ID = -1
# LOGIN_REDIRECT_URL = '/accounts/%(username)s/'
LOGIN_REDIRECT_URL = '/'
USERENA_SIGNIN_REDIRECT_URL = '/'
USERENA_SIGNIN_AFTER_SIGNUP = True
LOGIN_URL = '/accounts/signin/'
LOGOUT_URL = '/accounts/signout/'
AUTH_PROFILE_MODULE = 'accounts.MyProfile'
USERENA_WITHOUT_USERNAMES = True
USERENA_ACTIVATION_REQUIRED=False
USERENA_REDIRECT_ON_SIGNOUT = '/'
# AUTH_PROFILE_MODULE = "userprofile.userprofile"

SITE_TOP_NAME = "http://tenderokon.ru"
APPEND_SLASH = True

ADV_FOLDER = 'adv_images'

MIDDLEWARE_CLASSES = (
    'django_geoip.middleware.LocationMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'userena.middleware.UserenaLocaleMiddleware',
)

ROOT_URLCONF = 'multitender.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'multitender.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(SITE_ROOT, 'templates/'),

)

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS
TEMPLATE_CONTEXT_PROCESSORS += (
    'multitender.context_processors.banners',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    'django.contrib.admindocs',
    # 4 modules for logining
    'userena',
    'guardian',
    'easy_thumbnails',
    'accounts',
    'multitender',
    'tender',
    'sorl.thumbnail',
    # 'cities',
    'django_geoip',
    'south',
    'django.contrib.humanize',
    # 'debug_toolbar',
    'endless_pagination',  
    # 'masonry',  
)

# EMAIL_HOST = 'localhost'
# EMAIL_PORT = 1025

DEFAULT_FROM_EMAIL  = 'tender@tenderokon.ru' # send from e-mail address
EMAIL_HOST_USER     = 'tender@tenderokon.ru' # login if requared or ''
EMAIL_HOST_PASSWORD = 'Yp407pJB'       # password
EMAIL_HOST        = 'smtp.beget.ru' # SMTP server
EMAIL_PORT          = 2525     # SMTP server port (0 - use default port)
EMAIL_USE_TLS = False

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
