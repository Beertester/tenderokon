# -*- coding: utf-8 -*-
from django.template import Context, loader
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.core.mail import EmailMultiAlternatives, EmailMessage
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import simplejson
from django.core import serializers
from django.db.models import Q
from django.middleware.csrf import get_token
from django.template.loader import get_template
from django.template import Context
from django.core.urlresolvers import reverse
from django.db.models import Max, Min
from django.core import serializers
from django_geoip.models import City as Ccity
from multitender.func import *
from tender.func import *
from django.contrib.auth.models import User
from tender.forms import *
from multitender.forms import *
from django.contrib.auth import authenticate, login
from accounts.models import MyProfile
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings 
import json
import traceback
import re
import random
import time
import datetime

def home(request):    
    """Добавить новый тендер"""

    error = u'no'
    success = u'yes'

    sign_in_form = AuthenticationForm()

    client_ip, location, location_id = get_location(request)
    # client_city = get_user_city(request)
    
    example_citys = get_city_list(request=request, location=location, location_id=location_id)

    if request.method == 'POST':
        tender_form = NewTenderForm(data=request.POST, user=request.user)
        if tender_form.is_valid():
            name = tender_form.cleaned_data['name']
            email = tender_form.cleaned_data['email']
            phone = tender_form.cleaned_data['phone']
            city_select = tender_form.cleaned_data['city_select']
            city = tender_form.cleaned_data['city']
            # title = tender_form.cleaned_data['title']
            comment = tender_form.cleaned_data['comment']
            # tender_type = tender_form.cleaned_data['tender_type']            
            # activity = tender_form.cleaned_data['activity']            
            duration = tender_form.cleaned_data['duration']            

            city_instance = get_true_city(city_name = city_select, city_id = city)

            if request.user.is_authenticated and request.user.is_active:
                #TODO: Засунуть сюда проверку на пустой город
                if len(request.user.first_name)<=0 or len(request.user.my_profile.phone)<=0:
                    user_ins = request.user.get_profile()
                    data = {'name': name, 'phone': phone, 'privacy': user_ins.privacy}
                    signup_form = EditFormExtra(data, instance = user_ins)
                    if signup_form.is_valid():
                        user = signup_form.save()
                else:
                    user = request.user
            else:
                # Create New User and send email with pass
                password = password_generator()
                new_user_data = {
                    'email': email,
                    'password1': password,
                    'password2': password,
                    'name': name,
                    'role': 'client',
                    'phone': phone,
                    'city': city_instance.id,
                }
                user_form = HiddenSignupForm(new_user_data)
                if user_form.is_valid():
                    user = user_form.save()
                else:
                    print user_form.errors

                # print "New user profile", user.my_profile.id
                html_content = render_to_string('emails/new_user_new_tender.html',
                    { 'site_url': settings.SITE_TOP_NAME, 'user': user, 'password': password }
                )
                text_content = strip_tags(html_content)
                subject, from_email, to = 'Регистрация на сайте TenderOkon.ru', settings.DEFAULT_FROM_EMAIL, email
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()

                try:
                    user_to_login = authenticate(identification = email, password = password)
                except:
                    print traceback.format_exc()
                else:
                    try:
                        login(request, user_to_login)
                    except:
                        print traceback.format_exc()
                    else:
                        request.session.set_expiry(90 * 86400)

            save_tender_data = {
                'user': user.id, 
                # 'tender_type': tender_type, 
                # 'activity': activity, 
                'duration': duration,
                'comment': comment, 
                'city': city_instance.id,
            }
            save_tender_form = SaveTenderForm(save_tender_data)
            if save_tender_form.is_valid():
                stf = save_tender_form.save()

                # return redirect('my_tenders')
                return HttpResponseRedirect(reverse('show_tender', args=(stf.id,)))
            else:
                print save_tender_form.errors
    else:
        if request.user.is_authenticated and request.user.is_active:            
            try:
                pphone = request.user.my_profile.phone
            except:
                pphone = ''

            tender_form = NewTenderForm(initial = {
                'city_select': location, 
                'city': location_id, 
                'email': request.user.email,
                'name': request.user.first_name,
                'phone': pphone,
                })
        else:
            tender_form = NewTenderForm(initial = {'city_select': location, 'city': location_id})

    if request.user.is_authenticated and request.user.is_active:
        if len(request.user.first_name)>0:
            tender_form.fields['name'].widget.attrs['class'] = 'uneditable-input'
            tender_form.fields['name'].widget.attrs['readonly'] = 'readonly'
        if len(request.user.email)>0:
            tender_form.fields['email'].widget.attrs['class'] = 'uneditable-input'
            tender_form.fields['email'].widget.attrs['readonly'] = 'readonly'
        try:
            request.user.my_profile.phone
        except:
            pass
        else:
            if len(request.user.my_profile.phone)>0:
                tender_form.fields['phone'].widget.attrs['class'] = 'uneditable-input'
                tender_form.fields['phone'].widget.attrs['readonly'] = 'readonly'

    last_10_tenders = Tender.objects.all().order_by('-datetime_add')[:10]
    for tender in last_10_tenders:
        t = close_tender_by_time(request, tender) # искать в func.py 

        tender.you_company_in_party = you_company_in_party(request, tender)

        try:
            ccc = tender.datetime_finish
        except:
            time_to_finish = tender.datetime_add + datetime.timedelta(days=int(tender.duration)) - datetime.datetime.utcnow().replace(tzinfo=utc)
            tender.time_to_finish = time_to_finish.days*1440 + time_to_finish.seconds/60
        else:
            if ccc == None:
                time_to_finish = tender.datetime_add + datetime.timedelta(days=int(tender.duration)) - datetime.datetime.utcnow().replace(tzinfo=utc)
                tender.time_to_finish = time_to_finish.days*1440 + time_to_finish.seconds/60

        tender.party_count = TenderParty.objects.filter(tender=tender).count()
        tender.state = TenderState.objects.filter(tender=tender).order_by('-datetime_add')[:1]

    return render_to_response(
        'home.html', 
        {
            'error': error,
            'success': success,
            'request': request,
            'location': location,
            'client_ip': client_ip,
            'tender_form': tender_form,
            'sign_in_form': sign_in_form,
            'example_citys': example_citys,
            'last_10_tenders': last_10_tenders,
        },
        context_instance=RequestContext(request))     

def price_page(request):
    error = u'no'
    success = u'yes'

    return render_to_response(
        'price_page.html', 
        {
            'error': error,
            'success': success,
            'request': request,
        },
        context_instance=RequestContext(request))

def howitwork(request):
    error = u'no'
    success = u'yes'

    return render_to_response(
        'howitwork.html', 
        {
            'error': error,
            'success': success,
            'request': request,
        },
        context_instance=RequestContext(request))

def get_citys(request):
    return_data = []

    if request.method == "GET":
        getcity = str(request.GET['query'].encode('utf-8'))
        try:
            response_data = Ccity.objects.filter(name__icontains=getcity)
        except:
            print traceback.format_exc()
        else:
            for city in response_data:
                full_city_name = u'%s, %s' % (city.name, city.region.name)
                return_data.append({u'id': city.id, u'nname': full_city_name})

    return HttpResponse(json.dumps(return_data), content_type="application/json")

def check_mail(request):
    success = 0
    error = 0

    if request.method == 'POST':
        check_mail_form = CheckMailForm(request.POST)
        if check_mail_form.is_valid():
            try:
                mail_in_base = User.objects.get(email=check_mail_form.cleaned_data['email'])
            except:
                success = 2 #Пользователя нет такого
            else:
                success = 3 #Пользователь есть

    return_data = {}
    return_data['success'] = success
    return_data['error'] = error
    return HttpResponse(json.dumps(return_data), content_type="application/json")

def ajax_login(request):
    success = 0
    error = 0
    return_data = {}

    if request.method == 'POST':
        try:
            user = authenticate(identification = request.POST['identification'], password = request.POST['password'])
        except:
            # print traceback.format_exc()
            user = 0
        else:
            # print user
            try:
                login(request, user)
            except:
                # print traceback.format_exc()
                user = 0
            else:
                if user.is_authenticated() and user.is_active:
                    request.session.set_expiry(90 * 86400)
                    try:
                        user_profile = user.get_profile()
                    except:
                        # print traceback.format_exc()
                        success = 0
                    else:
                        success = 1 #Ура!
                        return_data['user'] = {}
                        return_data['user']['email'] = user_profile.user.email
                        if len(user_profile.phone)>0:
                            return_data['user']['phone'] = user_profile.phone
                        if len(user_profile.user.first_name)>0:
                            return_data['user']['name'] = user_profile.user.first_name
                        try:
                            city_id = user_profile.city.id
                        except:
                            client_ip, location, location_id = get_location(request)
                            return_data['user']['city'] = location_id
                            return_data['user']['city_select'] = location
                        else:
                            city_select = Ccity.objects.get(id = city_id)
                            return_data['user']['city'] = city_id
                            return_data['user']['city_select'] = city_select.name

    return_data['success'] = success
    return_data['error'] = error
    return HttpResponse(json.dumps(return_data), content_type="application/json")

def feedback_page(request):
    error = u'no'
    success = u'yes'

    form = FeedbackForm()

    if request.method == "POST":
        form = FeedbackForm(data = request.POST)
        if form.is_valid():
            form.save()

            client_ip, location, location_id = get_location(request)
            html_content = render_to_string('emails/feedback_email.html',{ 
                    'request': request,
                    'location': location,
                    'from_adress': form.cleaned_data['from_adress'],
                    'article': form.cleaned_data['article'],
                }
            )

            text_content = strip_tags(html_content)
            subject, from_email, to = 'Сообщение с TenderOkon.ru', settings.DEFAULT_FROM_EMAIL, 'yoginalex@gmail.com'
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()

        success = u'thanks'

    return render_to_response(
        'feedback_page.html', 
        {
            'error': error,
            'success': success,
            'request': request,
            'form': form,
        },
        context_instance=RequestContext(request))
