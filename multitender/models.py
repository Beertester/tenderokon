# -*- coding: utf-8 -*-
from django.db import models
import datetime

class Feedback(models.Model):
    datetime_add = models.DateTimeField(auto_now_add=True, 
                                        default=datetime.datetime.now, 
                                        verbose_name=u'Дата внесения в базу')    
    from_adress = models.CharField(max_length=250,
                            blank=True, 
                            verbose_name=u'Куда отвечать?')
    article = models.TextField(blank=True, 
                                verbose_name=u'Сообщение')

    def __unicode__(self):
        return u'%s' % self.article[:100]+'...'