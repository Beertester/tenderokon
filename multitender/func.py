# -*- coding: utf-8 -*-
from django_geoip.models import *
from tender.models import *
import random
import time
import datetime
import string
import traceback
import math

def get_location(request):
    location = request.location
    try:
        client_ip = request.META['HTTP_X_FORWARDED_FOR']
    except:
        client_ip = request.META['REMOTE_ADDR']

    try:
        geoip_record = IpRange.objects.by_ip(client_ip)
        location = u'%s, %s' % (geoip_record.city, geoip_record.region)
        location_id = geoip_record.city.id
    except IpRange.DoesNotExist:
        geoip_record = City.objects.get(name=u'Москва')
        location = u'%s, %s' % (geoip_record.name, geoip_record.region.name)
        location_id = geoip_record.id

    return client_ip,location,location_id

def password_generator(l = 8):
    characters = string.ascii_letters + string.digits
    password =  "".join(random.choice(characters) for x in range(l))
    return password

def get_true_city(city_name, city_id):
    try:
        city_by_id = City.objects.get(id=city_id)
    except:
        city_by_id = City.objects.get(name="Москва")

    if city_by_id.name != city_name.split(',')[0]:
        try:
            city_by_name = City.objects.get(name__startswith=city_name.split(',')[0])
        except:
            city_instance = city_by_id
        else:
            city_instance = city_by_name
    else:
        city_instance = city_by_id

    return city_instance

def get_city_list(request, location, location_id, num=4):
    city_list = {}

    geoip_record_m = City.objects.get(name="Москва")
    if location_id != geoip_record_m.id:
        location_m = u'%s, %s' % (geoip_record_m.name, geoip_record_m.region.name)
        location_m_s = u'%s' % geoip_record_m.name
        city_list[geoip_record_m.id] = {}
        city_list[geoip_record_m.id][0] = location_m_s
        city_list[geoip_record_m.id][1] = location_m        

    city_list[location_id] = {}
    city_list[location_id][0] = location.split(',')[0]
    city_list[location_id][1] = location

    if request.user.is_authenticated and request.user.is_active:
        try:
            user_city_id = request.user.my_profile.city.id
        except:
            print traceback.format_exc()
        else:
            if user_city_id not in city_list.keys():
                city_by_id = City.objects.get(id=user_city_id)
                city_list[city_by_id.id] = {}
                city_list[city_by_id.id][0] = u'%s' % city_by_id.name
                city_list[city_by_id.id][1] = u'%s, %s' % (city_by_id.name, city_by_id.region.name)

        try:
            user_tenders = Tender.objects.filter(user = request.user).order_by('city')[:num-2]
        except:
            print traceback.format_exc()
        else:
            for tender in user_tenders:
                try:
                   tender_city = tender.city.id
                except:
                    print traceback.format_exc()
                else: 
                    if tender_city not in city_list.keys():
                        city_by_id = City.objects.get(id=tender_city)
                        city_list[city_by_id.id] = {}
                        city_list[city_by_id.id][0] = u'%s' % city_by_id.name
                        city_list[city_by_id.id][1] = u'%s, %s' % (city_by_id.name, city_by_id.region.name)

    return city_list

def city_distance(lat1, lon1, lat2, lon2):
    #pi - число pi, rad - радиус сферы (Земли)
    # rad = 6373

    #координаты двух точек
    lat1 = float(lat1)
    lon1 = float(lon1)

    lat2 = float(lat2)
    lon2 = float(lon2)

    #в радианах
    # lat1 = llat1*math.pi/180.0
    # lat2 = llat2*math.pi/180.0
    # long1 = llong1*math.pi/180.0
    # long2 = llong2*math.pi/180.0

    # #косинусы и синусы широт и разницы долгот
    # cl1 = math.cos(lat1)
    # cl2 = math.cos(lat2)
    # sl1 = math.sin(lat1)
    # sl2 = math.sin(lat2)
    # delta = long2 - long1
    # cdelta = math.cos(delta)
    # sdelta = math.sin(delta)

    # #вычисления длины большого круга
    # y = math.sqrt(math.pow(cl2*sdelta,2)+math.pow(cl1*sl2-sl1*cl2*cdelta,2))
    # x = sl1*sl2+cl1*cl2*cdelta
    # ad = math.atan2(y,x)
    # dist = ad*rad

    # #вычисление начального азимута
    # x = (cl1*sl2) - (sl1*cl2*cdelta)
    # y = sdelta*cl2
    # z = math.degrees(math.atan(-y/x))

    # if (x < 0):
    #  z = z+180.

    # z2 = (z+180.) % 360. - 180.
    # z2 = - math.radians(z2)
    # anglerad2 = z2 - ((2*math.pi)*math.floor((z2/(2*math.pi))) )
    # angledeg = (anglerad2*180.)/math.pi

    # lat1, lon1 = origin
    # lat2, lon2 = destination
    radius = 6371 # km
 
    dlat = math.radians(lat2-lat1)
    dlon = math.radians(lon2-lon1)
    a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
        * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = radius * c
 
    return d

def get_user_city(request):
    if request.user.is_authenticated and request.user.is_active:
        try:
            city = request.user.my_profile.company.c_city
        except:
            try:
                city = request.user.my_profile.city
            except:
                client_ip, location, location_id = get_location(request)
                city = get_true_city(location, location_id)

    else:
        client_ip, location, location_id = get_location(request)
        city = get_true_city(location, location_id)

    return city