# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Feedback'
        db.create_table('multitender_feedback', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('datetime_add', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, auto_now_add=True, blank=True)),
            ('from_adress', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('article', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('multitender', ['Feedback'])


    def backwards(self, orm):
        # Deleting model 'Feedback'
        db.delete_table('multitender_feedback')


    models = {
        'multitender.feedback': {
            'Meta': {'object_name': 'Feedback'},
            'article': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'datetime_add': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'auto_now_add': 'True', 'blank': 'True'}),
            'from_adress': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['multitender']