# -*- coding: utf-8 -*-
from __future__ import with_statement
from fabric.contrib.console import confirm
from fabric.api import *
from fabric.contrib import django

django.settings_module('multitender.settings')
from django.conf import settings

env.hosts = ['yogina@axra.beget.ru']

def develop():
    # git --git-dir=../../tenderokon.git/ --work-tree=. pull origin master    
    # git reset --hard HEAD
    # git clean -f -d
    # git pull    
    local('git push origin master')

    with cd('/home/y/yogina/tenderokon.ru/public_html/'):
        run('git --git-dir=../../tenderokon.git/ --work-tree=. reset --hard HEAD')
        run('git --git-dir=../../tenderokon.git/ --work-tree=. clean -f -d')
        run('git --git-dir=../../tenderokon.git/ --work-tree=. pull origin master')
        run('git --git-dir=../../tenderokon.git/ --work-tree=. checkout master')

        run('rm multitender/settings.py')

        run('cp multitender/settings.production.py multitender/settings.py')
        
        # run('python manage.py syncdb')

        run('python manage.py collectstatic --noinput')

def syncdb():
    with cd('/home/y/yogina/tenderokon.ru/public_html/'):
        run('python manage.py syncdb')

def collstat():
    with cd('/home/y/yogina/tenderokon.ru/public_html/'):
        run('python manage.py collectstatic --noinput')

def spambase():
    # ./manage.py dumpdata --format json --indent 2 tender > tender.json    
    local('python manage.py reset --noinput accounts')
    local('python manage.py reset --noinput auth')
    local('python manage.py reset --noinput tender')
    local('python manage.py loaddata auth.json')
    local('python manage.py loaddata accounts.json')
    local('python manage.py loaddata tender.json')
    # local('python manage.py loadtestdata tender.Tender:100')
    local('python manage.py loadtestdata accounts.CompanyComment:30')
    local('python manage.py check_permissions')