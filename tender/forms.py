# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from accounts.models import *
from tender.models import *
from accounts.forms import *

class CheckMailForm(forms.Form):
    email = forms.EmailField(required=True)

class NewTenderForm(forms.Form):
    TENDER_TYPE = (
        ('dontknow', u'Не уверен'),
        ('production', u'Изготовление'),
        ('sale', u'Продажа'),
    )
    ACTIVITY = (
        ('dontknow', u'Не уверен'),
        ('plastic', u'Пластик'),
        ('polygraphy', u'Полиграфия'),        
    )
    DURATION = (
        ('3', u'срочный - 3 дня'),
        ('7', u'нормальный - 7 дней'),
        ('15', u'долгий - 15 дней'),
    )

    name = forms.CharField(label=u'Представьтесь *', 
            widget=forms.TextInput(
                attrs={"placeholder": u'Иванов Иван Иванович', 'required': 'required'}
            ),
            required=True
        )
    email = forms.EmailField(label=u'E-mail *',
            widget=forms.TextInput(
                attrs={"placeholder": u'ivanov.ivan@mail.ru', 'required': 'required'}
            ),
            required=True
        )
    phone = forms.CharField(label=u'Телефон',
            widget=forms.TextInput(
                attrs={"placeholder": u'+79267777777', }
            ),
            required=False
        )
    city_select = forms.CharField(label=u'Город *',
            widget=forms.TextInput(
                attrs={'required': 'required', 'data-provide': 'typeahead', 'class': 'typeahead full-width', 'autocomplete': 'off' }
            ),
            required=True
        )
    city = forms.CharField(label=u'Id города',
            widget=forms.HiddenInput(),
            required=True
        )        
    # title = forms.CharField(label=u'Название тендера',
    #         widget=forms.TextInput(
    #             attrs={"class": u'full-width', 'required': 'required'}
    #         ),
    #         required=False
    #     )
    # tender_type = forms.CharField(label=u'Тип тендера',
    #                         widget=forms.Select(choices=TENDER_TYPE),                      
    #                         required=True
    #     )
    # activity = forms.CharField(label=u'Направление',
    #                         # widget=forms.Select(choices=ACTIVITY), 
    #                         widget=forms.HiddenInput(),
    #                         required=True
    #     )
    duration = forms.CharField(label=u'Длительность тендера',
                            widget=forms.Select(choices=DURATION),
                            required=True,
                            initial='7'
        )
    # closed = forms.BooleanField(label=u'Закрытый тендер',
    #         widget=forms.CheckboxInput(attrs={"checked": "checked"}),
    #         initial=True,
    #         required=False,
    #     )   
    comment = forms.CharField(label=u'Описание тендера *',
            widget=forms.Textarea(
                attrs={"rows": 4, "class": u'full-width',}
            ),
            required=True
        )        

    def __init__(self, user=None, *args, **kwargs):
        super(NewTenderForm, self).__init__(*args, **kwargs)
        self._user = user

    def clean(self):
        cleaned_data = super(NewTenderForm, self).clean()
        email = cleaned_data.get('email')

        if not self._user.is_authenticated or not self._user.is_active:
            try:
                mail_in_base = User.objects.get(email=email)
            except:
                pass
            else:
                msg = u"Кто-то с такой почтой уже есть в базе, если это вы - залогиньтесь, пожалуйста.Или же укажите другую почту."
                self._errors["email"] = self.error_class([msg])
                raise forms.ValidationError("Кто-то с такой почтой уже есть в базе, если это вы - залогиньтесь, пожалуйста. Или же укажите другую почту.")

        return cleaned_data

class SaveTenderForm(forms.ModelForm):
    class Meta:
        model = Tender        
        exclude = ['datetime_add', ]
        
class TenderPartyForm(forms.Form):
    comment = forms.CharField(label=u'Комментарии',
            widget=forms.Textarea(
                attrs={"rows": 2, "class": u'full-width',}
            ),
            required=False
        )        

class FilterAllTendersForm(forms.Form):
    TENDER_TYPE = (
        ('all', u'Все'),
        ('production', u'Изготовление'),
        ('sale', u'Продажа'),
    )
    ACTIVITY = (
        ('all', u'Все'),
        ('plastic', u'Пластик'),
        ('polygraphy', u'Полиграфия'),        
    )

    # tender_type = forms.ChoiceField(label=u'Тип тендера',
    #                         widget=forms.RadioSelect(),
    #                         choices=TENDER_TYPE, 
    #                         initial='all',              
    #                         required=False
    #     )
    # activity = forms.ChoiceField(label=u'Направление',
    #                         widget=forms.RadioSelect(), 
    #                         choices=ACTIVITY,
    #                         initial='all',              
    #                         required=False
    #     )
    city_id = forms.CharField(widget=forms.HiddenInput(),
            required=False
        )        
    page = forms.CharField(widget=forms.HiddenInput(),
            required=False
        )        
    sort = forms.CharField(widget=forms.HiddenInput(),
            required=False
        )        

class FinalModalForm(forms.Form):
    tender = forms.IntegerField(required=True)
    rating = forms.IntegerField(required=True)
    company = forms.IntegerField(required=True)
    comment = forms.CharField(label=u'Комментарии',
            widget=forms.Textarea(
                attrs={"rows": 2, "class": u'full-width',}
            ),
            required=False
        )

class FinalChildCommentForm(forms.Form):
    taken = forms.IntegerField(required=True)
    comment_type = forms.CharField(required=True)
    comment = forms.CharField(label=u'Комментарии',
            widget=forms.Textarea(
                attrs={"rows": 2, "class": u'full-width',}
            ),
            required=False
        )
