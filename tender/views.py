# -*- coding: utf-8 -*-
from django.template import Context, loader, RequestContext
from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.core.mail import EmailMultiAlternatives, EmailMessage
from django.utils import simplejson
from django.db.models import Q
from django.middleware.csrf import get_token
from django.template.loader import get_template
from django.core.urlresolvers import reverse
from django.db.models import Max, Min
from django.core import serializers
from accounts.models import *
from tender.models import *
from multitender.func import *
from tender.func import *
from tender.forms import *
from userena.forms import *
from accounts.forms import EditFormExtra
from django.conf import settings 
from django.contrib.auth import authenticate, login
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.utils import formats
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json
from django.core.serializers.json import DjangoJSONEncoder
import traceback
import re
import random
import string
import time
import datetime

def all_tenders(request):
    error = u'no'
    success = u'yes'
    client_city = get_user_city(request)

    tenders = Tender.objects.all()
    try:
        sort_key = request.GET['sort']
    except:
        tenders = tenders.order_by('-datetime_add')
    else:
        if len(sort_key) > 0:
            if sort_key == 'timeup':
                tenders = tenders.order_by('-datetime_add')
            elif sort_key == 'timedown':
                tenders = tenders.order_by('datetime_add')
            else:
                tenders = tenders.order_by('-datetime_add')
        else:
            tenders = tenders.order_by('-datetime_add')

    # initial_data = {
    #     'activity': 'all',
    #     'tender_type': 'all'
    # }

    # Тут заполняем боковую форму фильтра
    filter_form = FilterAllTendersForm()
    if request.method == "GET":
        filter_form = FilterAllTendersForm(request.GET)
        if filter_form.is_valid():
            # if len(filter_form.cleaned_data["activity"]) > 0 and filter_form.cleaned_data["activity"] != 'all':
            #     tenders = tenders.filter(activity=filter_form.cleaned_data["activity"])

            # if len(filter_form.cleaned_data["tender_type"]) > 0 and filter_form.cleaned_data["tender_type"] != 'all':
            #     tenders = tenders.filter(tender_type=filter_form.cleaned_data["tender_type"])

            if len(filter_form.cleaned_data['city_id']) > 0 and filter_form.cleaned_data['city_id'] != all:
                if filter_form.cleaned_data['city_id'] != 'region':
                    try:
                        filter_city = City.objects.get(id=int(filter_form.cleaned_data['city_id']))
                    except:
                        pass
                    else:
                        tenders = tenders.filter(city=filter_city)
                else:
                    try:
                        region_city = City.objects.filter(region__id=client_city.region.id)
                    except:
                        print traceback.format_exc()
                    else:
                        tenders = tenders.filter(city__in=region_city)

    for tender in tenders:
        t = close_tender_by_time(request, tender) # искать в func.py 
        tender.party_count = TenderParty.objects.filter(tender=tender).count()

        tender.you_company_in_party = you_company_in_party(request, tender)

        tender_dist = city_distance(lat1=client_city.latitude, lon1=client_city.longitude, lat2=tender.city.latitude, lon2=tender.city.longitude)
        tender.distance = round(float(tender_dist),0)
        try:
            ccc = tender.datetime_finish
        except:
            time_to_finish = tender.datetime_add + datetime.timedelta(days=int(tender.duration)) - datetime.datetime.utcnow().replace(tzinfo=utc)
            tender.time_to_finish = time_to_finish.days*1440 + time_to_finish.seconds/60
        else:
            if ccc == None:
                time_to_finish = tender.datetime_add + datetime.timedelta(days=int(tender.duration)) - datetime.datetime.utcnow().replace(tzinfo=utc)
                tender.time_to_finish = time_to_finish.days*1440 + time_to_finish.seconds/60
        tender.state = TenderState.objects.filter(tender=tender).order_by('-datetime_add')[:1]
        
    try:
        sort_key = request.GET['sort']
    except:
        pass
    else:
        if len(sort_key) > 0:
            if sort_key == 'distup':
                tenders = sorted(tenders, key=lambda x: x.distance)
            if sort_key == 'distdown':
                tenders = sorted(tenders, key=lambda x: x.distance, reverse=True)

    return render_to_response(
        'all_tenders.html', 
        {
            'error': error,
            'success': success,
            'request': request,
            'tenders': tenders,
            'filter_form': filter_form,
        },
        context_instance=RequestContext(request))     

def my_tenders(request):
    error = u'no'
    success = u'yes'
    # client_ip, location, location_id = get_location(request)

    if request.user.is_authenticated and request.user.is_active:
        try:
            all_user_tenders = Tender.objects.filter(user = request.user).order_by('-datetime_add')
        except:
            print traceback.format_exc()
        else:
            for tender in all_user_tenders:
                tender.party_count = TenderParty.objects.filter(tender=tender).count()
                tender.final_comment = TenderFinalComment.objects.filter(tender=tender)
                tender.state = TenderState.objects.filter(tender=tender).order_by('-datetime_add')[:1]

                tender.you_company_in_party = you_company_in_party(request, tender)

                try:
                    ccc = tender.datetime_finish
                except:
                    time_to_finish = tender.datetime_add + datetime.timedelta(days=int(tender.duration)) - datetime.datetime.utcnow().replace(tzinfo=utc)
                    tender.time_to_finish = time_to_finish.days*1440 + time_to_finish.seconds/60
                else:
                    if ccc == None:
                        time_to_finish = tender.datetime_add + datetime.timedelta(days=int(tender.duration)) - datetime.datetime.utcnow().replace(tzinfo=utc)
                        tender.time_to_finish = time_to_finish.days*1440 + time_to_finish.seconds/60
                        
                try:
                    tender.party = TenderParty.objects.filter(tender=tender)
                except:
                    pass
                else:
                    for t_p in tender.party:
                        try:
                            tps = TenderPartySend.objects.filter(tender_party=t_p).count()
                        except:
                            t_p.send = False
                        else:
                            if tps >= 1:
                                t_p.send = True
                            else:
                                t_p.send = False

        return render_to_response(
            'all_my_tenders.html', 
            {
                'error': error,
                'success': success,
                'request': request,
                'tenders': all_user_tenders,
            },
            context_instance=RequestContext(request))     
    else:
        return HttpResponseRedirect(reverse('userena_signin'))

def company_tenders(request):
    error = u'no'
    success = u'yes'
    # client_ip, location, location_id = get_location(request)

    if (request.user.is_authenticated() and
            request.user.is_active and
            request.user.my_profile.company != None):
        try:
            tender_party = TenderParty.objects.filter(user=request.user).values('tender')
        except:
            print traceback.format_exc()
        else:
            all_company_tenders = Tender.objects.filter(id__in=tender_party).order_by('-datetime_add')

        for tender in all_company_tenders:
            tender.party_count = TenderParty.objects.filter(tender=tender).count()
            tender.state = TenderState.objects.filter(tender=tender).order_by('-datetime_add')[:1]

            try:
                ccc = tender.datetime_finish
            except:
                time_to_finish = tender.datetime_add + datetime.timedelta(days=int(tender.duration)) - datetime.datetime.utcnow().replace(tzinfo=utc)
                tender.time_to_finish = time_to_finish.days*1440 + time_to_finish.seconds/60
            else:
                if ccc == None:
                    time_to_finish = tender.datetime_add + datetime.timedelta(days=int(tender.duration)) - datetime.datetime.utcnow().replace(tzinfo=utc)
                    tender.time_to_finish = time_to_finish.days*1440 + time_to_finish.seconds/60

        return render_to_response(
            'all_company_tenders.html', 
            {
                'error': error,
                'success': success,
                'request': request,
                'tenders': all_company_tenders,
            },
            context_instance=RequestContext(request))     
    else:
        return HttpResponseRedirect(reverse('userena_signin'))


def show_tender(request, tender_id):
    error = u'no'
    success = u'yes'
    client_ip, location, location_id = get_location(request)

    tender = Tender.objects.get(id=tender_id)
    t = close_tender_by_time(request, tender) # искать в func.py 

    tender_party_form = TenderPartyForm()
    if request.method == "POST":
        tender_party_form = TenderPartyForm(request.POST)
        #TODO: Вставить проверку на возможность данного пользователя учавствовать в Тендерах
        if tender_party_form.is_valid():
            save_tender_party = TenderParty(
                user = request.user,
                tender = tender,
                comment = tender_party_form.cleaned_data['comment']
            )
            save_tender_party.save()    

    try:
        tender_party = TenderParty.objects.filter(tender=tender)
    except:
        tender_party = {}

    try:
        tender_state = TenderState.objects.filter(tender=tender).order_by('-datetime_add')
    except:
        tender_state = {}

    tender.tender_party = tender_party
    tender.tender_state = tender_state
    
    you_company_in_party = u'no'
    for party in tender_party:
        try:
            tender_party_send = TenderPartySend.objects.filter(tender_party=party).order_by('-datetime_add')
        except:
            pass
        else:
            party.send = tender_party_send

        if request.user.is_authenticated():
            try:
                ccom_id = request.user.my_profile.company.id
            except:
                you_company_in_party = u'no'
            else:
                if party.user.my_profile.company.id == request.user.my_profile.company.id:
                    you_company_in_party = u'yes'
                else:
                    you_company_in_party = u'no'
        else:
            you_company_in_party = u'no'

    return render_to_response(
        'current_tender.html', 
        {
            'error': error,
            'success': success,
            'request': request,
            'location': location,
            'client_ip': client_ip,
            'tender': tender,
            'tender_party_form': tender_party_form,
            'you_company_in_party': you_company_in_party,
            'now_time': datetime.datetime.now(),

        },
        context_instance=RequestContext(request))    

def stop_tender(request):
    success = 0
    error = 0

    if request.method == 'POST':
        try:
            tender = Tender.objects.get(id=request.POST["tender_id"])
        except:
            success = 0
            error = 1
        else:
            if tender.user == request.user:
                tender.datetime_finish = datetime.datetime.now()
                tender.save()
                party_count = TenderParty.objects.filter(tender=tender).count()
                if party_count > 0:
                    state = u'uclose'
                else:
                    state = u'final'
                ts = TenderState(user=request.user, tender=tender, state=state)
                ts.save()

                error = 0
                success = 1
            else:
                success = 0
                error = 2

    return_data = {}
    return_data['success'] = success
    return_data['error'] = error
    return HttpResponse(json.dumps(return_data), content_type="application/json")

def final_tender(request):
    success = 0
    error = 0

    if request.method == 'POST':
        ftf = FinalModalForm(request.POST)
        if ftf.is_valid():
            try:
                tender = Tender.objects.get(id=ftf.cleaned_data["tender"])
            except:
                success = 0
                error = 1
            else:
                try:
                    company = Company.objects.get(id=ftf.cleaned_data["company"])
                except:
                    success = 0
                    error = 2
                else:
                    try:
                        rating = int(ftf.cleaned_data["rating"])
                    except:
                        rating = 0

                    tender_final_comment = TenderFinalComment(
                            user = request.user,
                            company = company,
                            tender = tender,
                            comment = ftf.cleaned_data["comment"],
                            level = 0,
                            taken = False,
                            parent = None,
                            rating = rating
                        )
                    tender_final_comment.save()

                    success = 1
                    error = 0     
        else:
            error = 3
            success = 1       

    return_data = {}
    return_data['success'] = success
    return_data['error'] = error
    return HttpResponse(json.dumps(return_data), content_type="application/json")

def send_contacts(request):
    success = 1
    error = 0
    sending = {}

    if request.method == 'POST':
        try:
            tender = Tender.objects.get(id=request.POST['tender_id'])
        except:
            # print traceback.format_exc()
            error = 1
            success = 0
        else:
            try:
                tender_party = TenderParty.objects.filter(tender=tender)
            except:
                # print traceback.format_exc()
                error = 1
                success = 0
            else:
                for tp in tender_party:
                    if unicode(tp.id) in request.POST.getlist('companys[]'):
                        # Отправляем письмо, пишем в базу
                        html_content = render_to_string('emails/send_tender_contacts.html',
                            { 
                                'tender_party': tp, 
                                'site_url': settings.SITE_TOP_NAME,
                            }
                        )
                        text_content = strip_tags(html_content)
                        subject = u'Ответ по тендеру #%s' % tp.tender.id
                        from_email = settings.DEFAULT_FROM_EMAIL
                        to = tp.user.email
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        tps = TenderPartySend(tender_party=tp, user=request.user)
                        try:
                            tps.save()
                        except:
                            print traceback.format_exc()
                        else:
                            sending[tp.id] = formats.date_format(tps.datetime_add, "SHORT_DATETIME_FORMAT")

                success = 1

        # print request.POST.getlist('companys[]')

    return_data = {}
    return_data['success'] = success
    return_data['error'] = error
    return_data['sending'] = sending
    return HttpResponse(json.dumps(return_data, cls=DjangoJSONEncoder), content_type="application/json")
