# -*- coding: utf-8 -*-
import datetime
from django.db import models
from django.utils.translation import ugettext as _
from accounts.models import *
from tender.models import *
from django_geoip.models import City

class Tender(models.Model):
    TENDER_TYPE = (
        ('dontknow', u'Не уверен'),
        ('production', u'Изготовление'),
        ('sale', u'Продажа'),
    )
    ACTIVITY = (
        ('dontknow', u'Не уверен'),
        ('plastic', u'Пластик'),
        ('polygraphy', u'Полиграфия'),        
    )
    DURATION = (
        ('3', u'срочный - 3 дня'),
        ('7', u'нормальный - 7 дней'),
        ('15', u'долгий - 15 дней'),
    )
    datetime_add = models.DateTimeField(auto_now_add=True, 
                                        default=datetime.datetime.now, 
                                        verbose_name=u'Дата внесения в базу')    
    user = models.ForeignKey(User,
                                blank=False, 
                                null=False, 
                                verbose_name=u'Заказчик')
    # title = models.CharField(max_length=250,
    #                         blank=True, 
    #                         verbose_name=u'Название')
    # tender_type = models.CharField(choices=TENDER_TYPE, 
    #                         max_length=100,
    #                         blank=False,
    #                         # default=u'dontknow',
    #                         verbose_name=u'Тип тендера')
    # activity = models.CharField(choices=ACTIVITY, 
    #                         max_length=100,
    #                         blank=False,
    #                         # default=u'dontknow',
    #                         verbose_name=u'Направление тендера')
    #имеется в ввиду - закрыты участники тендера
    closed = models.BooleanField(default=True, 
                                    verbose_name=u'Тендер закрыт')    
    comment = models.TextField(blank=False, 
                                verbose_name=u'Комментарий')
    city = models.ForeignKey(City,
                            unique=False,
                            blank=True, 
                            null=True,
                            verbose_name=u'Город')
    duration = models.CharField(choices=DURATION, 
                                max_length=3,
                                blank=False,
                                # default='7',
                                verbose_name=u'Длительность тендера')
    datetime_finish = models.DateTimeField(blank=True,
                                        null=True, 
                                        default=None,
                                        verbose_name=u'Дата завершения тендера')    

    def __unicode__(self):
        return u'%s от %s' % (self.comment[:50]+'...', self.user.id)

    def save(self, *args, **kwargs):
        if not self.pk:
            new = True
        else:
            new = False
        super(Tender, self).save(*args, **kwargs)    

        if new:
            ts = TenderState(user=self.user, tender=self, state=u'new')
            ts.save()

class TenderState(models.Model):
    """Состояние тендера
    new - совсем новый,
    open - есть участники,
    work - заказчик посылал запросы участникам,
    tclose - закрыт по времени,
    uclose - закрыт заказчиком,
    final - финализирован заказчиком,
    nottaken - не принят участником,
    taken - принят участником
    """
    datetime_add = models.DateTimeField(auto_now_add=True, 
                                        default=datetime.datetime.now, 
                                        verbose_name=u'Дата внесения в базу')    
    user = models.ForeignKey(User,
                                blank=False, 
                                null=False, 
                                verbose_name=u'Кто поменял')
    tender = models.ForeignKey(Tender,
                                blank=False, 
                                null=False, 
                                verbose_name=u'Тендер')        
    state = models.CharField(max_length=250,
                            blank=False, 
                            verbose_name=u'Состояние')

    def __unicode__(self):
        return u'%s - %s' % (self.state, self.tender.comment[:50]+'...')

class TenderParty(models.Model):
    datetime_add = models.DateTimeField(auto_now_add=True, 
                                        default=datetime.datetime.now, 
                                        verbose_name=u'Дата внесения в базу')    
    user = models.ForeignKey(User,
                                blank=False, 
                                null=False, 
                                verbose_name=u'Исполнитель')
    tender = models.ForeignKey(Tender,
                                blank=False, 
                                null=False, 
                                verbose_name=u'Тендер')        
    comment = models.TextField(blank=True, 
                                verbose_name=u'Комментарий')

    def __unicode__(self):
        return u'%s - %s' % (self.tender.comment[:50]+'...', self.user.my_profile.company.c_title)

    def save(self, *args, **kwargs):
        super(TenderParty, self).save(*args, **kwargs)    

        ts = TenderState(user=self.user, tender=self.tender, state=u'open')
        ts.save()


class TenderPartySend(models.Model):
    datetime_add = models.DateTimeField(auto_now_add=True, 
                                        default=datetime.datetime.now, 
                                        verbose_name=u'Дата внесения в базу')    
    tender_party = models.ForeignKey(TenderParty,
                                    blank=False,
                                    null=False,
                                    verbose_name=u'Участник')
    user = models.ForeignKey(User,
                            blank=False, 
                            null=False, 
                            verbose_name=u'Пославший')

    def __unicode__(self):
        return u'%s - %s' % (self.datetime_add, self.tender_party)

    def save(self, *args, **kwargs):
        super(TenderPartySend, self).save(*args, **kwargs)    

        ts = TenderState(user=self.user, tender=self.tender_party.tender, state=u'work')
        ts.save()        

class TenderFinalComment(models.Model):
    datetime_add = models.DateTimeField(auto_now_add=True, 
                                        default=datetime.datetime.now, 
                                        verbose_name=u'Дата внесения в базу')    
    user = models.ForeignKey(User,
                            blank=False, 
                            null=False, 
                            verbose_name=u'Написал')
    company = models.ForeignKey(Company,
                                unique=False,
                                blank=True, 
                                null=True,
                                verbose_name=u'Компания')
    tender = models.ForeignKey(Tender,
                                blank=False, 
                                null=False, 
                                verbose_name=u'Тендер')        
    comment = models.TextField(blank=True, 
                                verbose_name=u'Комментарий')
    level = models.IntegerField(blank=False,
                                null=False,
                                default=0,
                                verbose_name=u'Уровень 0/1')
    taken = models.BooleanField(default=False,                                     
                                    verbose_name=u'Принят')
    parent = models.ForeignKey('TenderFinalComment',
                                unique=False,
                                blank=True, 
                                null=True,
                                default='Null',
                                verbose_name=u'Родитель')
    rating = models.IntegerField(blank=True,
                                null=True,
                                default=0,
                                verbose_name=u'Рейтинг')

    def __unicode__(self):
        return u'%s - %s' % (self.datetime_add, self.comment[:50]+'...')

    def save(self, *args, **kwargs):
        super(TenderFinalComment, self).save(*args, **kwargs)    

        state = u'final'
        if self.level == 0:
            state = u'final'
        else:
            if self.taken == False:
                state = u'nottaken'
            else:
                state = u'taken'            

        ts = TenderState(user=self.user, tender=self.tender, state=state)
        ts.save()        

class TenderFinalSend(models.Model):
    """Запросы на подтверждение участия"""
    datetime_add = models.DateTimeField(auto_now_add=True, 
                                        default=datetime.datetime.now, 
                                        verbose_name=u'Дата внесения в базу')    
    tender_party = models.ForeignKey(TenderParty,
                                    blank=False,
                                    null=False,
                                    verbose_name=u'Участник')
    user = models.ForeignKey(User,
                            blank=False, 
                            null=False, 
                            verbose_name=u'Пославший')

    def __unicode__(self):
        return u'%s - %s' % (self.datetime_add, self.tender_party)
