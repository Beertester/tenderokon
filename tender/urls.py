# -*- coding: utf-8 -*-
import os
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.conf import settings 

urlpatterns = patterns('',
    url(r'^show/(?P<tender_id>\d+)/$', 'tender.views.show_tender', name="show_tender"),    
    # url(r'^add/$', 'tender.views.add_tender', name='add_tender'),
    url(r'^all/$', 'tender.views.all_tenders', name='all_tenders'),
    url(r'^my/$', 'tender.views.my_tenders', name='my_tenders'),
    url(r'^company/$', 'tender.views.company_tenders', name='company_tenders'),
    url(r'^stop_tender/$', 'tender.views.stop_tender', name="stop_tender"),
    url(r'^final_tender/$', 'tender.views.final_tender', name="final_tender"),
    url(r'^send_contacts/$', 'tender.views.send_contacts', name="send_contacts")
)