# -*- coding: utf-8 -*-
from tender.models import *
from django.utils.timezone import utc
from django.contrib.auth.models import User
import time
import datetime
import string
import traceback
import math

def close_tender_by_time(request, tender):
    """Закрываем тендер по времени 
    Если у тендера нет дата_окончания
    Высчитываем теоретическую дату_закрытия 
    Сравниваем с Сейчас и пишем/не пишем

    Дополнительно дописываем в TenderState состояние тендера
    tclose - если есть участники, и заказчик еще может оценить работу
    final - если участников нет, и оценивать некого
    """
    try:
        tender = Tender.objects.get(id=tender)
    except:
        tender = tender

    if not tender.datetime_finish:
        tender_duration = int(tender.duration)
        tender_start_time = tender.datetime_add
        tender_close_time = tender_start_time + datetime.timedelta(days=tender_duration)

        if tender_close_time < datetime.datetime.utcnow().replace(tzinfo=utc):
            tender.datetime_finish = tender_close_time
            tender.save()
            party_count = TenderParty.objects.filter(tender=tender).count()
            if party_count > 0:
                state = u'tclose'
            else:
                state = u'final'

            if request.user.is_authenticated():
                user = request.user
            else:
                user = User.objects.get(id=-1)

            ts = TenderState(user=user, tender=tender, state=state)
            ts.save()

    return tender

def you_company_in_party(request, tender):
    you_company_in_party = u'no'
    try:
        tender_party = TenderParty.objects.filter(Q(user__my_profile__company=request.user.my_profile.company), Q(tender=tender))[0]
    except:
        you_company_in_party = u'no'
    else:
        if request.user.is_authenticated():
            try:
                ccom_id = request.user.my_profile.company.id
            except:
                you_company_in_party = u'no'
            else:       
                if tender_party.user.my_profile.company.id == request.user.my_profile.company.id:
                    you_company_in_party = u'yes'
                else:
                    you_company_in_party = u'no'
    return you_company_in_party
