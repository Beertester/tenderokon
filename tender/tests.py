# -*- coding: utf-8 -*-
"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.test.client import Client
from django import test
from django.core.urlresolvers import reverse

# class SimpleTest(TestCase):
#     def test_basic_addition(self):
#         """
#         Tests that 1 + 1 always equals 2.
#         """
#         self.assertEqual(1 + 1, 2)

# __test__ = {"urls": """
# >>> c = test.Client()
# >>> c.get(reverse('userena_signin')).status_code
# 200
# """}


class LoginTest(TestCase):
    """docstring for LoginTest"""
    def test_login(self):
        self.c = Client()
        response = self.c.post('/accounts/signin/', {'username': 'admin', 'password': 'admin'})
        self.assertEqual(response.status_code, 404)