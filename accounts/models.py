# -*- coding: utf-8 -*-
import datetime
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from userena.models import UserenaBaseProfile
from django_geoip.models import City
from django.db import models
from accounts.models import *
from PIL import Image as ImageLib
from PIL import ImageOps as ImageOpsLib
from django.conf import settings

class MyProfile(UserenaBaseProfile):
    ROLES = (
        ('zavod', u'Исполнитель'),
        ('client', u'Заказчик'),
    )
    user = models.OneToOneField(User,
                                unique=True,
                                verbose_name=_('user'),
                                related_name='my_profile')
    phone = models.CharField(max_length=250,
                                blank=True,
                                verbose_name=u'Телефон')    
    role = models.CharField(choices=ROLES, 
                            max_length=10, 
                            blank=False,
                            default=u'client',
                            verbose_name=u'Роль')
    master = models.BooleanField(default=False, 
                                    verbose_name=u'Мастер акк компании')    
    company = models.ForeignKey('Company',
                                unique=False,
                                blank=True, 
                                null=True,
                                verbose_name=u'Компания')
    city = models.ForeignKey(City,
                            unique=False,
                            null=True,
                            default=None,
                            verbose_name=u'Город')

    def __unicode__(self):
        return '%s (%s)' % (self.user.first_name, self.user.username)

class Company(models.Model):
    datetime_add = models.DateTimeField(auto_now_add=True, 
                                        default=datetime.datetime.now, 
                                        verbose_name=u'Дата внесения в базу')
    c_title = models.CharField(max_length=250,
                                blank=False, 
                                default=u'Компания',
                                verbose_name=u'Название')
    c_moderated = models.BooleanField(default=False, 
                                    verbose_name=u'Проверенно')
    c_email = models.EmailField(blank=False, 
                                null=False, 
                                max_length=200, 
                                default=u'empty@email.ru',
                                verbose_name=u'Почта')
    c_phone = models.CharField(max_length=250,
                                blank=True,
                                verbose_name=u'Телефон')
    c_site = models.CharField(max_length=250,
                            blank=True,
                            verbose_name=u'Сайт')
    c_city = models.ForeignKey(City,
                                unique=False,
                                null=True,
                                default=None,
                                verbose_name=u'Город')
    c_geo = models.CharField(max_length=250,
                            blank=True,
                            verbose_name=u'Координаты офиса')
    master_user = models.ForeignKey(User,
                                    blank=True,
                                    null=True,
                                    unique=False,
                                    related_name='master_user',
                                    verbose_name=u'Мастер аккаунт')    
    c_comment = models.TextField(blank=True, 
                                verbose_name=u'Комментарий')

    def __unicode__(self):
        return '%s' % self.c_title

class CompanyComment(models.Model):
    datetime_add = models.DateTimeField(auto_now_add=True, 
                                        default=datetime.datetime.now, 
                                        verbose_name=u'Дата внесения в базу')
    comment = models.TextField(blank=True, 
                                verbose_name=u'Комментарий')
    parent = models.ForeignKey('CompanyComment',
                                unique=False,
                                blank=True, 
                                null=True,
                                default=None,
                                verbose_name=u'Родитель')    
    company = models.ForeignKey('Company',
                                unique=False,
                                blank=False, 
                                null=False,
                                verbose_name=u'Компания')
    user = models.ForeignKey(User,
                            blank=False,
                            null=False,
                            unique=False,
                            verbose_name=u'Пользователь')  

    def __unicode__(self):
        return '%s - %s' % (self.user, self.comment[:50]+'...')

class CompanyAdversImage(models.Model):
    try:
        storage = settings.ADV_FOLDER+'/'
    except AttributeError:
        storage = 'adv_images/'

    datetime_add = models.DateTimeField(auto_now_add=True, 
                                        default=datetime.datetime.now, 
                                        verbose_name=u'Дата внесения в базу')
    user = models.ForeignKey(User,
                            blank=False,
                            null=False,
                            unique=False,
                            verbose_name=u'Пользователь')    
    shows = models.PositiveIntegerField(unique=False,
                                        default=0,
                                        blank=False,
                                        null=False,
                                        verbose_name=u'Кол-во показов') 
    company = models.ForeignKey(Company,
                                unique=False,
                                blank=False, 
                                null=False,
                                verbose_name=u'Компания')
    image = models.ImageField(upload_to=storage)
    url = models.URLField(blank=True,
                            null=True,
                            verbose_name=u'Ссылка в банер')
    
    def save(self, size=(468, 60)):
        if not self.id and not self.image:
            return
        
        super(CompanyAdversImage, self).save()        

        photo_width = self.image.width
        photo_height = self.image.height
        new_width = size[0]
        new_height = size[1]

        # only do this if the image needs resizing
        if (photo_width, photo_height) != (new_width, new_height):
            filename = str(self.image.path.encode('utf-8'))
            # print filename

            # filename = filename.encode('ascii', 'ignore')
            # print str(filename)

            image = ImageLib.open(filename)
            # photo_ratio = float(photo_width) / float(photo_height)
            # new_ratio = float(new_width) / float(new_height)
            image = ImageOpsLib.fit(image, size, ImageLib.ANTIALIAS)
            image.save(filename)

    def delete(self, *args, **kwargs):
        # You have to prepare what you need before delete the model
        storage, path = self.image.storage, self.image.path
        # Delete the model before the file
        super(CompanyAdversImage, self).delete(*args, **kwargs)
        # Delete the file after the model
        storage.delete(path)

    def __unicode__(self):
        return '%s' % (self.image.name)

