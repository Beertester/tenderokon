# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext as _
from userena.utils import get_profile_model, get_user_model
from django_geoip.models import City
from userena.forms import *
from accounts.models import *
from multitender.func import *


class EditFormExtra(forms.ModelForm):
    name = forms.CharField(label=u'Обращение', required=True)
    phone = forms.CharField(label=u'Телефон', required=False)    

    class Meta:
        model = MyProfile
        exclude = ['user', 'role', 'master', 'company', 'city',]

    def save(self):
        user_profile = super(EditFormExtra, self).save()
        user_profile.phone = self.cleaned_data['phone']
        user = user_profile.user
        user.first_name = self.cleaned_data['name']

        user.save()
        user_profile.save()

        return user

class HiddenSignupForm(SignupFormOnlyEmail):
    name = forms.CharField(required=True)
    phone = forms.CharField(required=False)
    role = forms.CharField(required=True)
    city = forms.CharField(required=True)

    def save(self):
        user = super(HiddenSignupForm, self).save()
        user_profile = user.my_profile
        
        if len(self.cleaned_data['phone'])>0:
            user_profile.phone = self.cleaned_data['phone']

        user_profile.role = self.cleaned_data['role']
        city = int(self.cleaned_data['city'])
        user_profile.city = City.objects.get(id=city)
        user_profile.save()

        user.first_name = self.cleaned_data['name']
        user.save()

        return user

class MainUserSignupForm(SignupFormOnlyEmail):
    name = forms.CharField(label=u'Представтесь *', 
            widget=forms.TextInput(
                attrs={"placeholder": u'Иванов Иван Иванович', 'required': 'required'}
            ),
            required=True
        )
    phone = forms.CharField(label=u'Телефон',
            widget=forms.TextInput(
                attrs={"placeholder": u'+79267777777', }
            ),
            required=False
        )
    city_select = forms.CharField(label=u'Город *',
            widget=forms.TextInput(
                attrs={'required': 'required', 'data-provide': 'typeahead', 'class': 'typeahead input-xlarge', 'autocomplete': 'off' }
            ),
            required=True
        )
    city = forms.CharField(label=u'id города',
            widget=forms.HiddenInput(),
            required=True
        )        
    role = forms.CharField(label=u'Роль',
            widget=forms.HiddenInput(),
            initial='client',
            required=True,
        )     
    master = forms.BooleanField(
            widget=forms.CheckboxInput(
                attrs={'style':'position:absolute;top:-9999px;left:-9999px;'}
            ),
            initial=False,
            required=False,
        )   

    # def __init__(self, *args, **kwargs):
    #     super(MainUserSignupForm, self).__init__(*args, **kwargs)
    #     self.request = kwargs.pop('request', None)
    #     client_ip, location, location_id = get_location(self.request)
    #     user_profile.city = get_true_city(
    #             city_id = self.cleaned_data['city'],
    #             city_name = self.cleaned_data['city_select']
    #         )

    def save(self):
        user = super(MainUserSignupForm, self).save()
        user_profile = user.my_profile
        user_profile.role = self.cleaned_data['role']
        user_profile.master = self.cleaned_data['master']
        user_profile.phone = self.cleaned_data['phone']

        user_profile.city = get_true_city(
                city_id = self.cleaned_data['city'],
                city_name = self.cleaned_data['city_select']
            )

        user_profile.save()

        user.first_name = self.cleaned_data['name']
        user.save()

        return user

class MiddleCompanySignupForm(forms.ModelForm):
    class Meta:
        model = Company
        exclude = ['datetime_add', 'c_moderated', ]

class MainCompanySignupForm(MainUserSignupForm):
    c_title = forms.CharField(label=u'Название *', 
        widget=forms.TextInput(
            attrs={"placeholder": u'ООО СуперФирма', 'required': 'required'}
        ),
        required=True
    )
    c_email = forms.EmailField(label=u'E-mail *',
            widget=forms.TextInput(
                attrs={"placeholder": u'super@firma.ru', 'required': 'required'}
            ),
            required=True
        )
    c_phone = forms.CharField(label=u'Телефон',
            widget=forms.TextInput(
                attrs={"placeholder": u'+79257777777', }
            ),
            required=False
        )
    c_site = forms.CharField(label=u'Сайт', 
        widget=forms.TextInput(
            attrs={"placeholder": u'supafirma.ru'}
        ),
        required=False
    )
    c_city_select = forms.CharField(label=u'Город',
            widget=forms.TextInput(
                attrs={'required': 'required', 'data-provide': 'typeahead', 'class': 'typeahead input-xlarge', 'autocomplete': 'off' }
            ),
            required=True
        )
    c_city = forms.CharField(label=u'id города',
            widget=forms.HiddenInput(),
            required=True
        )        
    c_comment = forms.CharField(label=u'Описание',
            widget=forms.Textarea(
                attrs={"rows": 4, "class": u'full-width',}
            ),
            required=False
        )  
    
    def clean(self):
        cleaned_data = super(MainCompanySignupForm, self).clean()
        email = cleaned_data.get('c_email')

        try:
            mail_in_base = Company.objects.get(email=email)
        except:
            pass
        else:
            msg = u"Компания с такой почтой уже есть в базе."
            self._errors["c_email"] = self.error_class([msg])
            raise forms.ValidationError("Компания с такой почтой уже есть в базе.")

        return cleaned_data

    def save(self):
        c_title = self.cleaned_data['c_title']
        c_email = self.cleaned_data['c_email']
        c_phone = self.cleaned_data['c_phone']
        c_site = self.cleaned_data['c_site']
        c_comment = self.cleaned_data['c_comment']

        c_city = get_true_city(
            city_name = self.cleaned_data['c_city_select'], 
            city_id = self.cleaned_data['c_city'],
        )

        new_company_data = {
            'c_title': c_title,
            'c_email': c_email,
            'c_phone': c_phone,
            'c_site': c_site,
            'c_city': c_city.id,
            'c_comment': c_comment,
        }
        
        company = MiddleCompanySignupForm(new_company_data)
        if company.is_valid():
            c = company.save(commit=False)
        
        user = super(MainCompanySignupForm, self).save()
        c.master_user = user
        c.save()

        user_profile = user.my_profile
        user_profile.company = c
        user_profile.save()

        return user

class MyEditProfileForm(forms.ModelForm):
    first_name = forms.CharField(label=u'Обращение',
                                 required=True)
    city_select = forms.CharField(label=u'Город',
            widget=forms.TextInput(
                attrs={'required': 'required', 'data-provide': 'typeahead', 'class': 'typeahead input-xlarge', 'autocomplete': 'off' }
            ),
            required=True
        )
    city = forms.CharField(label=u'id города',
            widget=forms.HiddenInput(),
            required=True
        )        

    def __init__(self, *args, **kw):
        super(MyEditProfileForm, self).__init__(*args, **kw)
        self.fields.keyOrder = ['first_name', 'phone', 'mugshot', 'city_select', 'city']

    class Meta:
        model = get_profile_model()
        exclude = ['user', 'privacy', 'role', 'master', 'company', 'city']

    def save(self, force_insert=False, force_update=False, commit=True):
        # self.cleaned_data['city'] = true_city
        new_city = get_true_city(
                city_id = self.cleaned_data['city'],
                city_name = self.cleaned_data['city_select']
            )

        profile = super(MyEditProfileForm, self).save(commit=commit)
        user = profile.user        
        user.first_name = self.cleaned_data['first_name']
        user.save()

        profile.city = new_city
        profile.save()

        return profile

class EditCompanyForm(forms.ModelForm):
    c_email = forms.EmailField(label=u'E-mail *',
            widget=forms.TextInput(
                attrs={"placeholder": u'super@firma.ru', 'required': 'required'}
            ),
            required=True
        )
    c_phone = forms.CharField(label=u'Телефон',
            widget=forms.TextInput(
                attrs={"placeholder": u'+79257777777', }
            ),
            required=False
        )
    c_site = forms.CharField(label=u'Сайт', 
        widget=forms.TextInput(
            attrs={"placeholder": u'supafirma.ru'}
        ),
        required=False
    )
    c_geo = forms.CharField(label=u'Координаты', 
        widget=forms.TextInput(
            attrs={"class": "uneditable-input"}
        ),
        required=False
    )
    c_comment = forms.CharField(label=u'Описание',
            widget=forms.Textarea(
                attrs={"rows": 4, }
            ),
            required=False
    )  

    class Meta:
        model = Company
        exclude = ['datetime_add', 'c_title', 'c_moderated', 'c_city', 'master_user', ]
        
class CompanyCommentForm(forms.ModelForm):
    class Meta:
        model = CompanyComment
        exclude = ['datetime_add', 'company', 'user', 'parent']

class UploadAdvForm(forms.Form):
    file  = forms.FileField(label=u'Рекламный баннер', required=False)        