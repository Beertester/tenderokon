# -*- coding: utf-8 -*-
from django.template import Context, loader, RequestContext
from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.core.files.uploadedfile import UploadedFile
from django.core.urlresolvers import reverse
from multitender.func import *
from userena.forms import *
from accounts.forms import *
from userena.views import *
from multitender.forms import *
from tender.forms import *
from userena import views as userena_views
from userena import signals as userena_signals
from django.core.urlresolvers import reverse
from userena import settings as userena_settings
import json
from django.core.serializers.json import DjangoJSONEncoder
import traceback
import re
import random
import string
import time
import datetime

def my_signup(request, signup_form=MainUserSignupForm, template_name='userena/signup_form.html', success_url=None, extra_context=None):

    client_ip, location, location_id = get_location(request)
    example_citys = get_city_list(request=request, location=location, location_id=location_id)
    
    return userena_views.signup(request=request, signup_form=signup_form, template_name=template_name, success_url=success_url, extra_context={'example_citys': example_citys, 'init_city_id': location_id, 'init_city_name': location})

def company_signup(request):
    error = u'no'
    success = u'yes'

    client_ip, location, location_id = get_location(request)
    example_citys = get_city_list(request=request, location=location, location_id=location_id)

    form = MainCompanySignupForm()

    if request.method == 'POST':
        form = MainCompanySignupForm(data = request.POST)
        if form.is_valid():
            user = form.save()

            userena_signals.signup_complete.send(sender=None, user=user)

            if request.user.is_authenticated():
                logout(request)

            if (userena_settings.USERENA_SIGNIN_AFTER_SIGNUP and
                not userena_settings.USERENA_ACTIVATION_REQUIRED):
                user = authenticate(identification=user.email, check_password=False)
                login(request, user)

            #TODO Redirect на страницу компании
            return redirect('all_tenders')

    return render_to_response(
        'company_signup.html', 
        {
            'error': error,
            'success': success,
            'request': request,
            'form': form,
            'example_citys': example_citys,
            'init_city_id': location_id, 
            'init_city_name': location,
        },
        context_instance=RequestContext(request))     

def show_company(request, company_id):
    error = u'no'
    success = u'yes'

    sign_in_form = AuthenticationForm()
    company = Company.objects.get(id=company_id)

    if request.method == 'POST':
        ccform = CompanyCommentForm(request.POST)
        if ccform.is_valid():
            cc = CompanyComment(comment=ccform.cleaned_data['comment'], 
                                company=company,
                                user=request.user)
            cc.save()

    company_comments = CompanyComment.objects.filter(company=company).order_by('-datetime_add', 'id')
    for cc in company_comments:
        cc.child = CompanyComment.objects.filter(parent=cc)

    company_tenders = TenderParty.objects.filter(user__my_profile__company=company)
    company_ratings = TenderFinalComment.objects.filter(company=company).filter(level=0)
    for cr in company_ratings:
        cr.child = TenderFinalComment.objects.filter(parent=cr)

    i = 0
    summ = 0
    for cr in company_ratings:

        try:
            r = int(cr.rating)
        except:
            pass
        else:
            summ = summ + r
            i = i + 1
            cr.rating_p = r * 20
    
    if i == 0:
        company.rating = 0
        company.rating_p = 0
    else:
        try:
            company.rating = float(summ)/i
        except:
            company.rating = 0
            company.rating_p = 0
        else:
            company.rating_p = company.rating * 20 ## 100 / 5 звезд - получаем процент :)

    return render_to_response(
        'show_company.html', 
        {
            'error': error,
            'success': success,
            'request': request,
            'company': company,
            'company_comments': company_comments,
            'company_tenders': company_tenders,
            'company_ratings': company_ratings,
            'sign_in_form': sign_in_form,
        },
        context_instance=RequestContext(request))     

# profile_edit(request, username, edit_profile_form=EditProfileForm,
#                  template_name='userena/profile_form.html', success_url=None,
#                  extra_context=None, **kwargs):

def my_profile_edit(request, username, edit_profile_form=MyEditProfileForm, template_name='userena/profile_form.html', success_url=None, extra_context=None):

    try:
        user = User.objects.get(username = username)
    except:
        client_ip, location, location_id = get_location(request)
    else:
        location = u'%s, %s' % (user.my_profile.city.name, user.my_profile.city.region.name)
        location_id = user.my_profile.city.id

    example_citys = get_city_list(request=request, location=location, location_id=location_id)

    return userena_views.profile_edit(request=request, username=username, edit_profile_form=edit_profile_form, template_name=template_name, success_url=success_url, extra_context={'example_citys': example_citys, 'init_city_id': location_id, 'init_city_name': location})

def edit_company(request, company_id):
    error = u'no'
    success = u'yes'

    company = Company.objects.get(id = company_id)

    if requst.user.is_authenticated() and request.user.my_profile.company == company:
        form = EditCompanyForm(instance = company)
        advform = UploadAdvForm()

        try:
            adv = CompanyAdversImage.objects.get(company=company)
        except:
            adv = ''

        if request.method == "POST":
            form = EditCompanyForm(request.POST, instance = company)
            advform = UploadAdvForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()
                
                if advform.is_valid():
                    file = advform.cleaned_data['file']

                    try:
                        wrapped_file = UploadedFile(file)
                    except:
                        pass

                    if wrapped_file:
                        filename = wrapped_file.name
                        try:
                            adv.id
                        except:
                            adv = CompanyAdversImage(image=file,
                                                    user=request.user,
                                                    company=company)
                            adv.save()
                        else:
                            adv.image = file
                            adv.user = request.user
                            adv.save()

    else:
        return redirect('show_company', company_id = company_id)

    return render_to_response(
        'edit_company.html', 
        {
            'error': error,
            'success': success,
            'request': request,
            'company': company,
            'form': form,
            'advform': advform,
            'adv': adv,
        },
        context_instance=RequestContext(request))     

def get_default_companys(request):
    """Helper for all_companys"""
    mylist = {}
    user_city = get_user_city(request)
    companys = Company.objects.filter(c_city__region=user_city.region)
    for c in companys:
        c.comments = CompanyComment.objects.filter(company=c).count()
        if c.c_city.id not in mylist.keys():
            mylist[c.c_city.id] = c.c_city

    return companys, mylist

def all_companys(request):
    error = u'no'
    success = u'yes'
    city_list = {}
    region_list = {}
    country_list = {}

    if 'top' in request.GET:
        companys = Company.objects.all()
        for c in companys:
            c.comments = CompanyComment.objects.filter(company=c).count()
            if c.c_city.region.country.code not in country_list.keys():
                country_list[c.c_city.region.country.code] = c.c_city.region.country
    elif 'country' in request.GET:
        try:
            companys = Company.objects.filter(c_city__region__country__code=request.GET['country'])
        except:
            companys, city_list = get_default_companys(request)
        else:
            for c in companys:
                c.comments = CompanyComment.objects.filter(company=c).count()
                if c.c_city.region.id not in region_list.keys():
                    region_list[c.c_city.region.id] = c.c_city.region
    elif 'region' in request.GET:
        try:
            companys = Company.objects.filter(c_city__region__id=int(request.GET['region']))
        except:
            companys, city_list = get_default_companys(request)
        else:
            for c in companys:
                c.comments = CompanyComment.objects.filter(company=c).count()
                if c.c_city.id not in city_list.keys():
                    city_list[c.c_city.id] = c.c_city
    elif 'city' in request.GET:
        try:
            companys = Company.objects.filter(c_city__id=int(request.GET['city']))
        except:
            # print traceback.format_exc()
            companys, city_list = get_default_companys(request)
        else:
            for c in companys:
                c.comments = CompanyComment.objects.filter(company=c).count()
                if c.c_city.id not in city_list.keys():
                    city_list[c.c_city.id] = c.c_city
    else:
        companys, city_list = get_default_companys(request)

    for c in companys:
        c.company_tenders = TenderParty.objects.filter(user__my_profile__company=c)
        c.company_ratings = TenderFinalComment.objects.filter(company=c)

        i = 0
        summ = 0
        for cr in c.company_ratings:
            try:
                r = int(cr.rating)
            except:
                pass
            else:
                summ = summ + r
                i = i + 1
        
        if i == 0:
            c.rating = 0
            c.rating_p = 0
        else:
            try:
                c.rating = float(summ)/i
            except:
                c.rating = 0
                c.rating_p = 0
            else:
                c.rating_p = c.rating * 20 ## 100 / 5 звезд - получаем процент :)

    return render_to_response(
        'all_companys.html', 
        {
            'error': error,
            'success': success,
            'request': request,
            'companys': companys,
            'city_list': city_list,
            'region_list': region_list,
            'country_list': country_list,
        },
        context_instance=RequestContext(request))     

def comment_company(request):
    success = 0
    error = 0
    comment_text = ''
    com_type = 0

    if request.method == 'POST':
        if request.POST['comment_type'] == 'simple':
            ccform = CompanyCommentForm(request.POST)
            if ccform.is_valid():
                try:
                    parent_id = request.POST['parent']
                except:
                    pass
                else:            
                    try:
                        parent = CompanyComment.objects.get(id=parent_id)
                    except:
                        error = 1
                        success = 0
                    else:
                        # Тут надо проверять является ли пользователь участником компании
                        cc = CompanyComment(comment=ccform.cleaned_data['comment'], 
                                            parent=parent,
                                            company=parent.company,
                                            user=request.user)
                        cc.save()
                        error = 0
                        success = 1
                        comment_text = cc.comment
                        comment_text = "<br />".join(comment_text.split("\n"))
        elif request.POST['comment_type'] == 'final':
            ccform = FinalChildCommentForm(request.POST)
            if ccform.is_valid():
                try:
                    parent_id = request.POST['parent']
                except:
                    error = 1
                    success = 0
                else:
                    try:
                        parent = TenderFinalComment.objects.get(id=parent_id)
                    except:
                        error = 2
                        success = 0
                    else:
                        try:
                            t = int(ccform.cleaned_data['taken'])
                        except:
                            taken = True
                        else:                            
                            if t == 1:
                                taken = True
                            elif t == 0:
                                taken = False
                            else:
                                taken = True

                        cc = TenderFinalComment(comment=ccform.cleaned_data['comment'],
                                                parent=parent,
                                                user=request.user,
                                                company=parent.company,
                                                level=1,
                                                tender=parent.tender,
                                                taken=taken)
                        cc.save()
                        error = 0
                        success = 1
                        if cc.taken:
                            com_type = 1
                        else:
                            com_type = 2
                        comment_text = cc.comment
                        comment_text = "<br />".join(comment_text.split("\n"))

    return_data = {}
    return_data['success'] = success
    return_data['error'] = error
    return_data['com_type'] = com_type    
    return_data['comment_text'] = comment_text
    return HttpResponse(json.dumps(return_data, cls=DjangoJSONEncoder), content_type="application/json")

