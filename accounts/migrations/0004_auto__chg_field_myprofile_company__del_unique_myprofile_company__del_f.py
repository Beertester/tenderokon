# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'MyProfile', fields ['company']
        db.delete_unique('accounts_myprofile', ['company_id'])


        # Changing field 'MyProfile.company'
        db.alter_column('accounts_myprofile', 'company_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounts.Company'], null=True))
        # Deleting field 'Company.comment'
        db.delete_column('accounts_company', 'comment')

        # Deleting field 'Company.site'
        db.delete_column('accounts_company', 'site')

        # Deleting field 'Company.phone'
        db.delete_column('accounts_company', 'phone')

        # Deleting field 'Company.city'
        db.delete_column('accounts_company', 'city_id')

        # Deleting field 'Company.master_profile'
        db.delete_column('accounts_company', 'master_profile_id')

        # Deleting field 'Company.title'
        db.delete_column('accounts_company', 'title')

        # Deleting field 'Company.moderated'
        db.delete_column('accounts_company', 'moderated')

        # Deleting field 'Company.email'
        db.delete_column('accounts_company', 'email')

        # Adding field 'Company.c_title'
        db.add_column('accounts_company', 'c_title',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041a\u043e\u043c\u043f\u0430\u043d\u0438\u044f', max_length=250),
                      keep_default=False)

        # Adding field 'Company.c_moderated'
        db.add_column('accounts_company', 'c_moderated',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Company.c_email'
        db.add_column('accounts_company', 'c_email',
                      self.gf('django.db.models.fields.EmailField')(default=u'empty@email.ru', unique=True, max_length=200),
                      keep_default=False)

        # Adding field 'Company.c_phone'
        db.add_column('accounts_company', 'c_phone',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=250, blank=True),
                      keep_default=False)

        # Adding field 'Company.c_site'
        db.add_column('accounts_company', 'c_site',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=250, blank=True),
                      keep_default=False)

        # Adding field 'Company.c_city'
        db.add_column('accounts_company', 'c_city',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['django_geoip.City'], null=True),
                      keep_default=False)

        # Adding field 'Company.master_user'
        db.add_column('accounts_company', 'master_user',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='master_user', null=True, to=orm['auth.User']),
                      keep_default=False)

        # Adding field 'Company.c_comment'
        db.add_column('accounts_company', 'c_comment',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)


    def backwards(self, orm):

        # Changing field 'MyProfile.company'
        db.alter_column('accounts_myprofile', 'company_id', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['accounts.Company'], unique=True, null=True))
        # Adding unique constraint on 'MyProfile', fields ['company']
        db.create_unique('accounts_myprofile', ['company_id'])

        # Adding field 'Company.comment'
        db.add_column('accounts_company', 'comment',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)

        # Adding field 'Company.site'
        db.add_column('accounts_company', 'site',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=250, blank=True),
                      keep_default=False)

        # Adding field 'Company.phone'
        db.add_column('accounts_company', 'phone',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=250, blank=True),
                      keep_default=False)

        # Adding field 'Company.city'
        db.add_column('accounts_company', 'city',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['django_geoip.City'], null=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Company.master_profile'
        raise RuntimeError("Cannot reverse this migration. 'Company.master_profile' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'Company.title'
        raise RuntimeError("Cannot reverse this migration. 'Company.title' and its values cannot be restored.")
        # Adding field 'Company.moderated'
        db.add_column('accounts_company', 'moderated',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Company.email'
        raise RuntimeError("Cannot reverse this migration. 'Company.email' and its values cannot be restored.")
        # Deleting field 'Company.c_title'
        db.delete_column('accounts_company', 'c_title')

        # Deleting field 'Company.c_moderated'
        db.delete_column('accounts_company', 'c_moderated')

        # Deleting field 'Company.c_email'
        db.delete_column('accounts_company', 'c_email')

        # Deleting field 'Company.c_phone'
        db.delete_column('accounts_company', 'c_phone')

        # Deleting field 'Company.c_site'
        db.delete_column('accounts_company', 'c_site')

        # Deleting field 'Company.c_city'
        db.delete_column('accounts_company', 'c_city_id')

        # Deleting field 'Company.master_user'
        db.delete_column('accounts_company', 'master_user_id')

        # Deleting field 'Company.c_comment'
        db.delete_column('accounts_company', 'c_comment')


    models = {
        'accounts.company': {
            'Meta': {'object_name': 'Company'},
            'c_city': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['django_geoip.City']", 'null': 'True'}),
            'c_comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'c_email': ('django.db.models.fields.EmailField', [], {'default': "u'empty@email.ru'", 'unique': 'True', 'max_length': '200'}),
            'c_moderated': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'c_phone': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'c_site': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'c_title': ('django.db.models.fields.CharField', [], {'default': "u'\\u041a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f'", 'max_length': '250'}),
            'datetime_add': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'master_user': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'master_user'", 'null': 'True', 'to': "orm['auth.User']"})
        },
        'accounts.myprofile': {
            'Meta': {'object_name': 'MyProfile'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['django_geoip.City']", 'null': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['accounts.Company']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'master': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mugshot': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'privacy': ('django.db.models.fields.CharField', [], {'default': "'registered'", 'max_length': '15'}),
            'role': ('django.db.models.fields.CharField', [], {'default': "u'client'", 'max_length': '10'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'my_profile'", 'unique': 'True', 'to': "orm['auth.User']"})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'django_geoip.city': {
            'Meta': {'unique_together': "(('region', 'name'),)", 'object_name': 'City'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '9', 'decimal_places': '6', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '9', 'decimal_places': '6', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cities'", 'to': "orm['django_geoip.Region']"})
        },
        'django_geoip.country': {
            'Meta': {'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '2', 'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'django_geoip.region': {
            'Meta': {'unique_together': "(('country', 'name'),)", 'object_name': 'Region'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'regions'", 'to': "orm['django_geoip.Country']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['accounts']