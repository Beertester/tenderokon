# -*- coding: utf-8 -*-
import os
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.conf import settings 

urlpatterns = patterns('',
    url(r'^signup/$', 'accounts.views.company_signup', name='company_signup'),
    url(r'^(?P<company_id>\d+)/$', 'accounts.views.show_company', name="show_company"),    
    url(r'^(?P<company_id>\d+)/edit/$', 'accounts.views.edit_company', name="edit_company"),    
    url(r'^all/$', 'accounts.views.all_companys', name='all_companys'),
    url(r'^comment_company/$', 'accounts.views.comment_company', name='comment_company'),
)